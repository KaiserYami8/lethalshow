﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WaveManager : MonoBehaviour
{
    public Wave[] waves;

    public int waveInd = -1;
    public int nbActive = 0;

    public float transitionTime;

    public Animator alert;
    public Animator win;

    void Update()
    {
        //Get Enemies on Scene
        nbActive = 0;
        foreach (EnemyController ec in FindObjectsOfType<EnemyController>())
        {
            if (ec.es != null)
                if (ec.gameObject.activeSelf)
                    nbActive += ec.es.bulletNb;
        }
        foreach (PelushBehavior pel in FindObjectsOfType<PelushBehavior>())
        {
            if (pel.gameObject.activeSelf)
                nbActive += 1;
        }
        //If no enemy shoot, clean, next wave and spawn
        if (nbActive <= 0 && FindObjectOfType<GameManager>().combos == 0)
        {
            if (waveInd+1 == waves.Length)
            {
                win.SetTrigger("Win");
            }
            else
            {
                transitionTime -= Time.deltaTime;
                foreach (EnemyController ec in FindObjectsOfType<EnemyController>())
                    if (ec.gameObject.activeSelf)
                        ec.gameObject.transform.Translate(-Vector3.up);

                if (transitionTime <= 0)
                {
                    foreach (EnemyController ec in FindObjectsOfType<EnemyController>())
                        if (ec.gameObject.activeSelf)
                            ec.gameObject.SetActive(false);

                    waveInd++;

                    if (waveInd+1 == waves.Length)
                        alert.SetTrigger("Alert");

                    transitionTime = 1f;
                    Spawn();
                }
            }
        }
    }

    void Spawn()
    {
        foreach (EnemyGroupSpawn sp in waves[waveInd].spawners)
            sp.Spawn();
    }
}

[System.Serializable]
public class Wave
{
    public EnemyGroupSpawn[] spawners;
}