﻿using UnityEngine;

public class PlayerReflect : MonoBehaviour
{
    string input;
    float timer;
    float strgth;
    float charge;

    PlayerController pl;
    GameManager gm;

    public ParticleSystem hit;

    private void Start()
    {
        pl = transform.parent.GetComponent<PlayerController>();
        gm = FindObjectOfType<GameManager>();
    }

    void Update()
    {
        //Disable on Reflect End
        timer -= Time.deltaTime;
        if (timer <= 0)
            gameObject.SetActive(false);
    }

    public void Spawn(float _timer, float _strght, float _charge, string _input)
    {
        timer = _timer;
        strgth = _strght;
        charge = _charge;
        input = _input;
    }

    private void OnTriggerEnter(Collider collision)
    {
        //If Hit another Player
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.parent.GetComponent<PlayerController>().Heal();
        }

        //If Hit Bullet not from Player
        if (collision.gameObject.layer == 8 && collision.GetComponent<Bullet>().lastPlayer != transform.parent.GetComponent<PlayerController>().playerIndex)
        {
            //Get bullet collided
            var bul = collision.GetComponent<Bullet>();    
            var bulRb = collision.GetComponent<Rigidbody>();

            //Change Color
            collision.GetComponent<Renderer>().material = bul.mats[pl.playerIndex];
            bul.lastPlayer = pl.playerIndex;

            //Set BulletLife
            bul.life = 3;
            
            if (input == "Smash")
            {
                bulRb.velocity = (Vector3.forward + Vector3.right * Input.GetAxis("P" + pl.controllerIndex + pl.controllerType + "_HMove") / 6).normalized * strgth;
                bul.isGoingStraight = true;
                bulRb.useGravity = false;

                if (bul.combo >= 5)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        if (i != 3)
                        {
                            var b = Instantiate(collision.gameObject, collision.transform.position, Quaternion.identity);
                            b.GetComponent<Rigidbody>().velocity = (Vector3.forward * ((i % 4) / 3) + Vector3.right * ((i - 3) / 3)).normalized * strgth;
                            b.GetComponent<Bullet>().combo = 3;
                            //b.GetComponent<Bullet>().lastPlayer = bul.lastPlayer;
                        }
                    }
                    FindObjectOfType<ZoomEffect>().zoomPlayer[pl.playerIndex - 1] = true;
                }

                if (bul.isCombo)
                {
                    //Set Target Position
                    FindObjectOfType<TargetBullet>().bullet = null;
                    FindObjectOfType<GameManager>().onCombo = false;
                }

                hit.Play();

                FindObjectOfType<AudioScript>().Play("Ball");
            }

            var playerSide = -((pl.playerIndex - 1) * 2 - 1);
            if (input == "Lob")
            {
                //Add Combo
                if (bul.isCombo)
                    bul.combo++;
                else if (!gm.onCombo)
                {
                    bul.isCombo = true;
                    bul.combo++;
                    gm.onCombo = true;
                    gm.bulletCombo = collision.gameObject;
                }
                if (bul.combo > gm.combos)
                {
                    gm.combos = bul.combo;
                    gm.ComboUp();
                }

                if (bul.isCombo)
                {
                    //Clamp Combo from +1f/+Inf to +2f/+1f
                    float comboT = (1f + 1f / (1f + bul.combo / 5f));
                    //Set Vertical Force
                    var forceV = new Vector3(0, 4.905f * comboT, 0);
                    //Get Time to fall back
                    float time = (forceV.y * (1f + 1f / comboT)) / 9.81f;
                    //Set Horizontal Speed
                    var forceH = Vector3.right * playerSide * ((Mathf.Abs(collision.transform.position.x) + charge * 6) / time);

                    var forceD = (Vector3.forward * Input.GetAxis("P" + pl.controllerIndex + pl.controllerType + "_VMove") * 2) / time;

                    //Set Target Position
                    FindObjectOfType<TargetBullet>().PredictLob(forceV + forceH + forceD, collision.gameObject);

                    //Set Bullet Velocity
                    bulRb.velocity = forceH + forceV + forceD;
                    bulRb.useGravity = true;
                }
                else
                {
                    var m = bulRb.velocity.magnitude;
                    bulRb.velocity = (bulRb.velocity - Vector3.right * playerSide * strgth / 8).normalized * strgth;
                }

                FindObjectOfType<AudioScript>().Play("Ball");
            }

            bul.Reflected();

            //Freeze Time
            FindObjectOfType<TimeManager>().StartCountStopTime();
        }
    }
}