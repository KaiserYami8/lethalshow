﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FadeBlockingUI : MonoBehaviour
{
    public Transform player;
    public Transform targetBullet;

    float alpha;

    GraphicRaycaster m_Raycaster;
    PointerEventData m_PointerEventData;
    EventSystem m_EventSystem;

    void Start()
    {
        m_Raycaster = GetComponent<GraphicRaycaster>();
        m_EventSystem = GetComponent<EventSystem>();
    }
    
    void Update()
    {
        alpha = 1;

        m_PointerEventData = new PointerEventData(m_EventSystem);
        
        m_PointerEventData.position = Camera.main.WorldToScreenPoint(player.position);

        List<RaycastResult> results = new List<RaycastResult>();

        m_Raycaster.Raycast(m_PointerEventData, results);

        foreach (RaycastResult result in results)
        {
            alpha = .2f;
        }

        m_PointerEventData.position = Camera.main.WorldToScreenPoint(targetBullet.position);

        results = new List<RaycastResult>();

        m_Raycaster.Raycast(m_PointerEventData, results);

        foreach (RaycastResult result in results)
        {
            alpha = .2f;
        }

        GetComponent<CanvasGroup>().alpha = Mathf.Lerp(GetComponent<CanvasGroup>().alpha, alpha, .3f);
    }
}
