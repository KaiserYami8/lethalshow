﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    public EnemyShoot es;

    public ParticleSystem[] psBites;

    public GameObject go;

    public void StartSound(string soundName)
    {
        FindObjectOfType<AudioScript>().Play(soundName);
    }

    public void StopSound(string soundName)
    {
        FindObjectOfType<AudioScript>().Stop(soundName);
    }

    public void LoadScene(int index)
    {
        FindObjectOfType<LevelLoader>().LoadLevel(index);
    }

    public void Shoot()
    {
        es.Shoot();
    }

    public void Tongue()
    {
        transform.parent.GetComponent<PelushBehavior>().Catch();
    }

    public void Bite()
    {
        foreach(ParticleSystem sb in psBites)
        {
            sb.Play();
        }
    }

    public void SetInactive()
    {
        go.SetActive(false);
    }
}