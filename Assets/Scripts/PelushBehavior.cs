﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PelushBehavior : MonoBehaviour
{
    public GameObject[] collars;

    public GameObject tongue;
    float timerTongue = 20;
    Vector3 localTonguePos;

    public Animator animator;

    public bool transformed, dead;

    float invincibility;

    public ParticleSystem psBreak;

    public GameObject eyeS1, eyeS2;


    void Start()
    {
        localTonguePos = tongue.transform.localPosition;

        GetComponent<EnemyController>().es.enabled = true;
    }
    
    void Update()
    {
        invincibility -= Time.deltaTime;

        if (timerTongue < 10)
        {
            timerTongue -= Time.deltaTime;
            if (timerTongue <= 0)
            {
                tongue.transform.DOLocalMove(localTonguePos, .2f);
                timerTongue = 20;
            }
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            Damage(5);
        }
    }

    public void Attack()
    {
        float rnd = Random.value;
        if (transformed)
        {
            if (rnd < .6f)
                animator.SetTrigger("Shoot");
            else
                animator.SetTrigger("Cac");

            Debug.LogWarning("say hi : " + rnd);
        }
        else
        {
            /*if (rnd < .4f)
                transform.DOMoveX(10, .5f);
            else if (rnd < .8f)*/
            animator.SetTrigger("Shoot");
            /*else
                animator.SetTrigger("Gobe");*/

            Debug.LogWarning("not transfo");
        }
    }

    public void Damage(int combo)
    {

        if (combo > 1 && invincibility <= 0)
        {
            invincibility = .5f;


            for (int i = 0; i < 3; i++)
            {
                if (collars[i] != null)
                {
                    psBreak.transform.position = collars[i].transform.position;
                    psBreak.Play();
                    collars[i].SetActive(false);
                    collars[i] = null;

                    if (i == 2)
                    {
                        StartCoroutine(Transfo());
                        invincibility = 3f;
                    }

                    animator.SetTrigger("Hurt");
                    FindObjectOfType<AudioScript>().Play("EnemyDead");

                    return;
                }
            }
            if (combo > 4)
            {
                animator.SetTrigger("Death");
                FindObjectOfType<AudioScript>().Play("EnemyDead");
                eyeS1.SetActive(false);
                eyeS2.SetActive(false);
                dead = true;
            }

        }
        else
            FindObjectOfType<AudioScript>().Play("BallDeflect");
    }

    public void Catch()
    {
        timerTongue = .6f;
        tongue.transform.DOMove(FindObjectOfType<PlayerController>().transform.position + Vector3.up, .6f);
    }

    IEnumerator Transfo()
    {
        yield return new WaitForSeconds(1f);

        animator.SetTrigger("Transfo");
        transformed = true;

        eyeS1.SetActive(true);
        eyeS2.SetActive(true);

        yield return null;
    }
}
