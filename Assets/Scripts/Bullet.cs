﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet : MonoBehaviour
{
    public Material[] mats;

    public float size;

    public int life;
    bool isDead;

    public int lastPlayer;
    public int combo;

    public Text comboTxt;

    public Gradient[] playerColors;

    public bool isCombo;

    Rigidbody rb;
    public ParticleSystem psElectricity, psCircle1, psCircle2;
    public TrailRenderer trail;
    public Transform parent;

    public bool isGoingStraight;

    float timeOnGround = 10f;

    public ParticleSystem flash;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        Initiate();
    }

    public void Initiate()
    {
        rb.isKinematic = false;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        
        rb.useGravity = false;
        life = 3;
        isDead = false;
        lastPlayer = 0;
        isCombo = false;
        combo = 1;

        GetComponent<MeshRenderer>().enabled = true;
        GetComponent<SphereCollider>().enabled = true;
        trail.enabled = true;
    }

    public void Sleep()
    {
        if (isCombo)
        {
            var gm = FindObjectOfType<GameManager>();
            gm.onCombo = false;
            gm.bulletCombo = null;
            gm.combos = 0;
        }

        transform.parent = parent;
        transform.position = parent.transform.position;
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<SphereCollider>().enabled = false;
        trail.enabled = false;

        comboTxt.text = null;

        GetComponent<Renderer>().material = mats[0];
        trail.colorGradient = playerColors[0];
        var em = psElectricity.emission;
        em.rateOverTime = 0;
        psCircle1.Stop();
        psCircle2.Stop();

        GetComponent<Bullet>().enabled = false;
    }

    private void LateUpdate()
    {
        //Speed deformation
        if (life > 0)  
            transform.localScale = new Vector3(1, 1, Mathf.Clamp(rb.velocity.magnitude / 5, 1, 2)) * size;
    }

    void Update()
    {
        if (lastPlayer > 0)
        {
            //Change Color
            var color = psElectricity.main.startColor;
            color.colorMin = playerColors[lastPlayer].Evaluate(0);
            color.colorMax = playerColors[lastPlayer].Evaluate(100);
            trail.colorGradient = playerColors[lastPlayer];
            //Change Aura
            var em = psElectricity.emission;
            em.rateOverTime = combo * 5;
        }

        size = .4f * (1 + combo / 10f);

        //AutoAim
        if (FindObjectOfType<EnemyController>() != null && lastPlayer != 0 && rb.velocity.y == 0)
        {
            GameObject nearestEn = null;
            for (int i = 0; i < FindObjectsOfType<EnemyController>().Length; i++)
            {
                GameObject en = FindObjectsOfType<EnemyController>()[i].gameObject;
                if (nearestEn == null)
                {
                    //distance
                    if (Vector3.Distance(en.transform.position, transform.position) < 20)
                        nearestEn = en;
                }
                else
                {
                    if (Vector3.Distance(en.transform.position, transform.position) < Vector3.Distance(nearestEn.transform.position, transform.position))
                        nearestEn = en;
                }
            }

            if (nearestEn != null)
            {
                //Power
                float x = (nearestEn.transform.position.x - transform.position.x) * 0.1f;
                rb.velocity += new Vector3(x, 0, 0);
            }
        }

        //Rotate toward Direction
        if (rb.velocity.magnitude > 0)
            transform.rotation = Quaternion.LookRotation(rb.velocity, Vector3.up);

        //Set Shadow
        transform.GetChild(0).transform.localEulerAngles = new Vector3(90, transform.transform.eulerAngles.y, transform.transform.eulerAngles.z);
        transform.GetChild(0).transform.position = new Vector3(transform.transform.position.x, 0.01f, transform.transform.position.z);

        //Destroy if far
        if (transform.position.z < -7 || transform.position.z > 27)
            rb.useGravity = true;

        //Text Face Camera
        comboTxt.gameObject.transform.LookAt(FindObjectOfType<Camera>().transform.position + FindObjectOfType<Camera>().transform.forward * 100);

        //Display Combo above Bullet
        if (combo > 1)
            comboTxt.text = "x" + combo;
        else
            comboTxt.text = null;

        //Go Straight
        if (isGoingStraight)
        {
            float acc = .2f;
            if (Mathf.Abs(rb.velocity.x) > .1f)
            {
                float dir = rb.velocity.x / Mathf.Abs(rb.velocity.x);
                Vector3 v = new Vector3(rb.velocity.x - Time.deltaTime * acc * dir, 0, rb.velocity.z + Time.deltaTime * acc);
                rb.velocity = v;
            }
            else
            {
                Vector3 v = new Vector3(0, 0, rb.velocity.z + Mathf.Abs(rb.velocity.x));
                rb.velocity = v;
                isGoingStraight = false;
            }
        }

        //DestroyOnGround()
        if (timeOnGround <= 1f)
            timeOnGround -= Time.deltaTime;
        if (timeOnGround <= 0)
        {
            timeOnGround = 10f;
            life = 0;
            IsItDead();
        }
    }

    #region Collisions

    private void OnCollisionEnter(Collision collision)
    {
        //Hit Static
        if (collision.gameObject.tag == "Wall")
        {
            life--;
            if (lastPlayer == 0)
                life = 0;

            //FindObjectOfType<AudioScript>().Play("Ball");
        }
        if (collision.gameObject.tag == "Ground")
        {
            //life = 0;
            timeOnGround = .2f;
            rb.velocity = Vector3.zero;
        }
        IsItDead();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Hit Forcefield
        if (other.gameObject.tag == "ForceField" && lastPlayer != 0)
        {
            if (combo > 2)
            {
                other.gameObject.SetActive(false);
                FindObjectOfType<AudioScript>().Play("ShieldDown");
            }
            else
            {
                FindObjectOfType<AudioScript>().Play("BallDeflect");
                life = 0;
            }
        }

        //Hit Player
        if (other.gameObject.tag == "Player" && lastPlayer == 0 && !isDead)
        {
            other.transform.parent.GetComponent<PlayerController>().Damaged();
            life = 0;

            //FindObjectOfType<AudioScript>().Play("Ball");
        }

        //Hit Enemy
        if (other.gameObject.tag == "Enemy" && lastPlayer != 0 && !isDead)
        {
            if (other.transform.parent.GetComponent<PelushBehavior>() != null)
            {
                other.transform.parent.GetComponent<PelushBehavior>().Damage(combo);
            }
            else
            {
                other.transform.parent.gameObject.SetActive(false);
                other.transform.parent.GetComponent<EnemyController>().es.enabled = false;
                FindObjectOfType<AudioScript>().Play("EnemyDead");
            }
            if (combo >= 5)
                FindObjectOfType<GameManager>().effect = true;
            else
                flash.Play();
            life = 0;
        }

        IsItDead();
    }

    void IsItDead()
    {
        if (life <= 0 && !isDead)
        {
            GetComponent<Animator>().SetTrigger("Death");
            rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
            rb.isKinematic = true;
            isDead = true;
        }
    }

    #endregion

    public void Reflected()
    {
        //Reflected Effects
        var main1 = psCircle1.main;
        main1.startColor = playerColors[lastPlayer].Evaluate(50);
        psCircle1.Play();
        var main2 = psCircle2.main;
        main2.startColor = playerColors[lastPlayer].Evaluate(50);
        psCircle2.Play();

        timeOnGround = 10f;
        if (transform.position.y < .5f)
        {
            Vector3 pos = transform.position;
            pos.y = .5f;
            transform.position = pos;
        }
    }

    public void Destroy()
    {
        Sleep();
    }
}
