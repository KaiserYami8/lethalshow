﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public int playerIndex;
    public int controllerIndex;
    public char controllerType;
    Rigidbody rb;

    public string charaName;
    
    [Header("Movement")]
    public float maxSpd;
    public float acc;
    public float spd, spdMult;

    public float x;
    public float z;
    public Vector3 dir;
    
    [Header("Strike")]
    public GameObject hitboxStrike;
    public float releaseTime;
    float recoveryTime;
    public float strenght;

    bool isCharging;
    float chargeTime;

    [Space]

    public ParticleSystem psGlow;
    public ParticleSystem psStrech;
    
    [Header("Damaged")]
    public ParticleSystem psElectricty;
    public bool isDamaged;
    float invinciblility = 0f;
    Vector3 shake = new Vector3(.1f, 0, -.1f);

    public Presentateur animateur;

    public Renderer[] lines;
    public Material lineDamage, lineDefault;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        spd = maxSpd;
        spdMult = 1;
        isDamaged = false;
        isCharging = false;
    }

    void Update()
    {
        //Get Input
        //dir = new Vector3(Mathf.Round(Input.GetAxis("P" + controllerIndex + "_HMove")), 0, -Mathf.Round(Input.GetAxis("P" + controllerIndex + "_VMove")));
        x = Input.GetAxis("P" + controllerIndex + controllerType + "_HMove");
        z = Input.GetAxis("P" + controllerIndex + controllerType + "_VMove");
        dir.Set(Mathf.Round(x), 0f, Mathf.Round(z));

        //Reset Speed By Time
        spdMult = Mathf.Lerp(spdMult, 1, .1f);

        //Move
        rb.velocity = dir.normalized * (spd * .01667f) * spdMult;
        //rb.AddForce(dir.normalized * (spd * .01667f) * spdMult, ForceMode.Force);

        recoveryTime -= Time.deltaTime;

        //Strike
        if (!isDamaged && recoveryTime <= 0)
        {
            //Start Charge
            if (Input.GetButton("P" + controllerIndex + controllerType + "_Shoot") && !isCharging)
                Charge("Smash");
            if (Input.GetButton("P" + controllerIndex + controllerType + "_Lob") && !isCharging)
                Charge("Lob");

            //Charging
            if ((Input.GetButton("P" + controllerIndex + controllerType + "_Shoot") || Input.GetButton("P" + controllerIndex + controllerType + "_Lob")) && isCharging)
            {
                spdMult = 1 / (chargeTime * 2 + 1) + .1f;

                chargeTime = Mathf.Clamp(chargeTime + Time.deltaTime, 0, 1);

                var main = psGlow.main;
                main.startSize = chargeTime * 4;

                psStrech.GetComponent<ParticleSystemRenderer>().velocityScale = chargeTime;
            }

            //Strike
            if (Input.GetButtonUp("P" + controllerIndex + controllerType + "_Shoot"))
                Strike("Smash");
            if (Input.GetButtonUp("P" + controllerIndex + controllerType + "_Lob"))
                Strike("Lob");
        }

        if (isDamaged)
        {
            //Shake mesh
            transform.GetChild(0).Translate(shake);
            shake *= -1;
            //rb.velocity = Vector3.zero;
        }
        invinciblility -= Time.deltaTime;
        if (invinciblility % .2f >= .1f)
            transform.GetChild(0).gameObject.SetActive(false);
        else
            transform.GetChild(0).gameObject.SetActive(true);
    }

    public void Charge(string typeOfStrike)
    {
        transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetTrigger(typeOfStrike);
        transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetBool("Charge", true);
        psGlow.Play();
        psStrech.Play();
        isCharging = true;
        
        var main = psGlow.main;
        main.startSize = 0;
        
        animateur.Anim("Shock", true, false);
        FindObjectOfType<AudioScript>().Play("ArmSmash_" + charaName + Random.Range(1, 3));
        FindObjectOfType<AudioScript>().Play("Energy");

    }

    public void Strike(string typeOfStrike)
    {
        //Dash to Target
        if (Vector3.Distance(FindObjectOfType<TargetBullet>().transform.position, transform.position) < 4f)
            rb.AddForce((FindObjectOfType<TargetBullet>().transform.position - transform.position) * chargeTime * 20, ForceMode.Impulse);

        hitboxStrike.SetActive(true);
        hitboxStrike.GetComponent<PlayerReflect>().Spawn(releaseTime, strenght, chargeTime, typeOfStrike);

        chargeTime = 0;
        transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetBool("Charge", false);
        chargeTime = 0;
        psGlow.Stop();
        psStrech.Stop();

        isCharging = false;
        recoveryTime = .6f;
        
        animateur.Anim("Idle", false, false);
        FindObjectOfType<AudioScript>().Play("ArmSmash_" + charaName + Random.Range(1, 3));
        FindObjectOfType<AudioScript>().Stop("Energy");
    }

    public void Damaged()
    {
        if (!isDamaged && invinciblility <= 0)
        {
            chargeTime = 0;
            transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetBool("Charge", false);
            chargeTime = 0;
            psGlow.Stop();
            psStrech.Stop();

            psElectricty.Play();
            transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetBool("Hurt", true);
            isDamaged = true;

            foreach (Renderer l in lines)
                l.material = lineDamage;

            animateur.Anim("Ouch", true, false);
            FindObjectOfType<GameManager>().mixer.SetFloat("lowpassMusic", 800);
            FindObjectOfType<AudioScript>().Play("Player_Death");
            FindObjectOfType<AudioScript>().Stop("Energy");

            int damaged = 0;
            foreach(PlayerController pl in FindObjectsOfType<PlayerController>())
            {
                if (pl.isDamaged)
                    damaged++;
            }
            if (damaged == 2)
                FindObjectOfType<GameManager>().Lose();
        }
        else
        {
            spdMult = -1;
        }
    }

    public void Heal()
    {
        if (isDamaged)
        {
            psElectricty.Stop();
            transform.GetChild(0).GetChild(0).GetComponent<Animator>().SetBool("Hurt", false);
            isDamaged = false;
            invinciblility = .8f;

            FindObjectOfType<GameManager>().mixer.SetFloat("lowpassMusic", 22000);

            foreach (Renderer l in lines)
                l.material = lineDefault;

            animateur.Anim("Happy", true, false);
            FindObjectOfType<AudioScript>().Play("Player_Heal");
        }
    }
}
