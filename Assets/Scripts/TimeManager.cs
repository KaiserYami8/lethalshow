﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public float stopWaitTime = 0.1f;
    public int stopFrame = 2;
    float stopTimeScale = 1;
    int counter;

    void Start()
    {
        Application.targetFrameRate = 100;
    }
    
    void Update()
    {
        if (counter > 0)
        {
            stopTimeScale = 0;
            counter--;
        }
        else
        {
            stopTimeScale = 1;
        }
        Time.timeScale = stopTimeScale;
    }
    void StartStopFrame()
    {
        counter = stopFrame;
    }
    public void StartCountStopTime()
    {
        Invoke("StartStopFrame", stopWaitTime);
    }
}
