﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public int combos;

    public Text combotxt;

    public bool onCombo;
    public GameObject bulletCombo;
    public GameObject bulletTarget;

    public GameObject[] players;
    public GameObject[] meshesCharas;

    public bool[] controllerAssigned;

    public AudioMixer mixer;

    public Slider volMaster, volMusic, volSFX;

    public CanvasGroup pause;

    public bool effect;
    public PostProcessingProfile ppMain, ppSaturate;
    public Light lSaturate;
    public ParticleSystem[] particlesFlash;

    public Animator lose;

    private void Start()
    {
        onCombo = false;

        controllerAssigned = new bool[4];

        for (int i = 0; i < players.Length; i++)
        {
            GameObject mesh = null;

            if (FindObjectOfType<GlobalScript>() != null)
            {
                if (FindObjectOfType<GlobalScript>().playerChara[i] == "Chara1")
                {
                    mesh = meshesCharas[0];
                    players[i].GetComponent<PlayerController>().charaName = "cc";
                }
                if (FindObjectOfType<GlobalScript>().playerChara[i] == "Chara2")
                {
                    mesh = meshesCharas[1];
                    players[i].GetComponent<PlayerController>().charaName = "t";
                }

                Instantiate(mesh, players[i].transform.GetChild(0).transform.position, Quaternion.identity, players[i].transform.GetChild(0));
            }
            else
            {
                Instantiate(meshesCharas[i], players[i].transform.GetChild(0).transform.position, Quaternion.identity, players[i].transform.GetChild(0));
            }
        }
        
        /*FindObjectOfType<AudioScript>().Play("FightTheme");
        FindObjectOfType<AudioScript>().Play("Ready");
        FindObjectOfType<AudioScript>().Play("PublicStadium");*/
    }

    private void Update()
    {
        /*if (Input.GetKey(KeyCode.Joystick1Button0) && !controllerAssigned[0])
            AssignController(1);

        if (Input.GetKey(KeyCode.Joystick2Button0) && !controllerAssigned[1])
            AssignController(2);*/

        /*if (Input.GetKey(KeyCode.Joystick1Button7) || Input.GetKey(KeyCode.Joystick2Button7))
            foreach (GameObject pl in players)
                pl.GetComponent<PlayerController>().controllerType = 'c';*/

        if (Input.GetKeyDown(KeyCode.G) || Input.GetKeyDown(KeyCode.M))
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
                GetComponent<TimeManager>().enabled = false;
                mixer.GetFloat("volumeMaster", out float i);
                mixer.SetFloat("volumeMaster", i - 10);
                pause.alpha = 1;
            }
            else
            {
                Time.timeScale = 1;
                GetComponent<TimeManager>().enabled = true;
                mixer.GetFloat("volumeMaster", out float i);
                mixer.SetFloat("volumeMaster", i + 10);
                pause.alpha = 0;
            }


        if (Input.GetKey(KeyCode.Space))
            foreach (GameObject pl in players)
                pl.GetComponent<PlayerController>().controllerType = 'k';

        /*mixer.SetFloat("volumeMaster", volMaster.value);
        mixer.SetFloat("volumeMusic", volMusic.value);
        mixer.SetFloat("volumeSFX", volSFX.value);*/

        if (effect)
        {
            StartCoroutine(SaturateEffect());
            effect = false;
        }
        
        Debug();
    }

    public void PredictLob(Vector3 velocity)
    {
        float time = (velocity.y * 2 + bulletCombo.transform.position.y * 1.5f) / 9.81f;
        float movement = velocity.x * time;

        bulletTarget.transform.position = new Vector3(bulletCombo.transform.position.x + movement, bulletTarget.transform.position.y, bulletCombo.transform.position.z);

        bulletTarget.GetComponent<TargetBullet>().bullet = bulletCombo;
    }

    public void Lose()
    {
        lose.SetTrigger("Lose");
        //FindObjectOfType<LevelLoader>().LoadLevel(1);
    }

    void AssignController(int controllerInd)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetComponent<PlayerController>().controllerIndex == 0)
            {
                players[i].GetComponent<PlayerController>().controllerIndex = controllerInd;
                controllerAssigned[controllerInd - 1] = true;
                players[i].GetComponent<PlayerController>().enabled = true;
                break;
            }
        }
    }

    public void ComboUp()
    {
        //combotxt.text = "Combo x" + combos;
    }

    void Debug()
    {
        if (Input.GetKeyDown(KeyCode.R))
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        //if (Input.GetKeyDown(KeyCode.C))
        //FindObjectOfType<WaveManager>().transitionTime = 2f;
        /*foreach (EnemyGroupSpawn sp in spawners)
            sp.spawn = true;*/

        if (Input.GetKeyDown(KeyCode.N))
        {
            foreach (EnemyController ec in FindObjectsOfType<EnemyController>())
            {
                if (ec.gameObject.activeSelf)
                    ec.gameObject.SetActive(false);
            }
        }
    }

    IEnumerator SaturateEffect()
    {
        Camera.main.GetComponent<PostProcessingBehaviour>().profile = ppSaturate;
        lSaturate.enabled = true;
        lSaturate.transform.DOMoveY(2, .4f);

        foreach (ParticleSystem ps in particlesFlash)
            ps.Play();

        yield return new WaitForSeconds(.3f);

        lSaturate.color = new Color(1f, .71f, 0f);

        ColorGradingModel.Settings colorStg = ppSaturate.colorGrading.settings;
        colorStg.basic.postExposure = -20;
        colorStg.basic.saturation = 1;
        ppSaturate.colorGrading.settings = colorStg;

        yield return new WaitForSeconds(.2f);

        Camera.main.GetComponent<PostProcessingBehaviour>().profile = ppMain;
        lSaturate.enabled = false;
        lSaturate.transform.DOMoveY(6, 0);

        colorStg.basic.postExposure = -13;
        colorStg.basic.saturation = 0;
        ppSaturate.colorGrading.settings = colorStg;

        yield return null;
    }
}