﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGroupSpawn : MonoBehaviour
{
    public GameObject enemyPrefab;
    
    public int nbToSpawn;

    public bool spawn;

    public Transform[] trackSpawn, trackLoop;

    public List<GameObject> children;
    
    void Start()
    {
        children = new List<GameObject>();

        for (int i = 0; i < nbToSpawn; i++)
            Initialize();
    }
    
    void Update()
    {
        if (spawn)
        {
            Spawn();
            spawn = false;
        }
    }

    void Initialize()
    {
        GameObject en = Instantiate(enemyPrefab, trackSpawn[0].position, Quaternion.identity, transform);

        if (en.GetComponent<EnemyController>() != null)
        {
            en.GetComponent<EnemyController>().trackSpawn = trackSpawn;
            en.GetComponent<EnemyController>().trackLoop = trackLoop;
        }

        en.SetActive(false);

        children.Add(en);
    }

    public void Spawn()
    {
        FindEnemy().SetActive(true);
    }

    public GameObject FindEnemy()
    {
        foreach (GameObject en in children)
        {
            if (!en.activeSelf)
                return en.gameObject;
        }

        return null;
    }
}