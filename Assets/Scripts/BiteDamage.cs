﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiteDamage : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //Hit Player
        if (other.gameObject.tag == "Player")
        {
            other.transform.parent.GetComponent<PlayerController>().Damaged();
        }
    }
}
