﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{
    public bool isShooting;

    public GameObject bullet;

    public bool isAiming;

    public float wait;
    public int bulletNb;
    public float rateOfFire;
    public float angleOfFire;

    float bulletAngle, rateOfFireRemain;

    public GameObject mesh;
    public Animator animator;

    public bool boss;

    List<GameObject> bullets;

    public PelushBehavior isPelush;


    void Start()
    {
        bullets = new List<GameObject>();

        rateOfFireRemain = rateOfFire;

        InstantiateBulletPool();
    }

    public void InstantiateBulletPool()
    {
        foreach (Transform child in transform)
        {
            for (int i = 0; i <= Mathf.Ceil(1 / rateOfFire * 3) * bulletNb * 1.5f; i++)
            {
                var bul = Instantiate(bullet, child.position, Quaternion.identity, child);
                bul.GetComponent<Bullet>().parent = child;
                bul.GetComponent<Bullet>().enabled = false;

                bullets.Add(bul);
                bul.GetComponent<Bullet>().Sleep();
            }
        }
    }
    
    void Update()
    {
        //Aim Nearest Player if isAiming
        AimNearestPlayer();

        //Shoot
        wait -= Time.deltaTime;
        if (wait <= 0)
            TryShoot();
            //animator.SetTrigger("Shoot");
    }

    void AimNearestPlayer()
    {
        //Target Nearest Player
        Transform p = null;
        float dist = 100;
        foreach (PlayerController pc in FindObjectsOfType<PlayerController>())
        {
            if (Vector3.Distance(pc.transform.position, transform.position) < dist)
            {
                p = pc.transform;
                dist = Vector3.Distance(pc.transform.position, transform.position);
            }
        }
        //Weapon LookAt
        if (p != null && isAiming)
        {
            Debug.DrawRay(transform.position, p.position - transform.position, Color.red, Time.deltaTime);
            transform.LookAt(p);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
        //Rotate Mesh Toward Aim
        mesh.transform.rotation = transform.rotation;
    }

    public void TryShoot()
    {
        rateOfFireRemain -= Time.deltaTime;
        bulletAngle += Time.deltaTime * 180;

        if (rateOfFireRemain <= 0)
        {
            if (isPelush != null)
            {
                isPelush.Attack();
            }
            else
            {
                animator.SetTrigger("Shoot");

                Debug.LogWarning("ES Shoot : " + transform.parent.name);
            }


            rateOfFireRemain = rateOfFire;
        }
    }

    public void Shoot()
    {
        foreach (Transform child in transform)
        {
            for (float i = 0; i < bulletNb; i++)
            {
                //Find Free Bullet
                var bSpawn = FindBullet(child);

                bulletAngle = angleOfFire * ((i + 1) - bulletNb / 2 - .5f);
                bSpawn.transform.eulerAngles = child.eulerAngles + new Vector3(0, bulletAngle, 0);

                bSpawn.transform.position = child.transform.position + new Vector3(0, .5f, 0);

                bSpawn.GetComponent<Bullet>().enabled = true;
                //bSpawn.GetComponent<Bullet>().Initiate();
                bSpawn.GetComponent<Bullet>().transform.parent = null;

                bSpawn.GetComponent<Rigidbody>().velocity = bSpawn.transform.forward * 12;
            }
        }
        rateOfFireRemain = rateOfFire;
        //animator.SetTrigger("Shoot");
        FindObjectOfType<AudioScript>().Play("EnemyShoot" + Random.Range(1, 5));
    }
    
    public GameObject FindBullet(Transform parent)
    {
        foreach (Transform bullet in parent)
        {
            if (!bullet.GetComponent<Bullet>().enabled)
                return bullet.gameObject;
        }

        return null;
    }
}