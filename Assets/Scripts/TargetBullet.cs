﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBullet : MonoBehaviour
{
    public GameObject bullet;

    void Start()
    {
        
    }
    
    void Update()
    {
        if (bullet != null)
        {
            float scale = .99f;
            //if (bullet.GetComponent<Rigidbody>().velocity.y < 0)
                scale = .99f + Mathf.Max(bullet.transform.position.y / 2 - 1f, 0);

            if (bullet.GetComponent<Rigidbody>().velocity.y == 0)
                scale = .99f;
                
            transform.GetChild(0).localScale = new Vector3(scale, scale, scale);

            if (!bullet.GetComponent<MeshRenderer>().enabled)
            {
                bullet = null;
                transform.GetChild(4).GetComponent<ParticleSystem>().Play();
            }
        }
        else
        {
            transform.position = new Vector3(100, 0.02f, 0);
            transform.GetChild(0).localScale = new Vector3(.99f, .99f, .99f);
        }
    }

    public void PredictLob(Vector3 velocity, GameObject bulletCombo)
    {
        Vector3 bPos = bulletCombo.transform.position;

        float time = (velocity.y * 2 + bPos.y * 1.5f) / 9.81f;
        float movement = velocity.x * time;
        float depth = velocity.z * time;

        transform.position = new Vector3(bPos.x + movement, transform.position.y, bPos.z + depth);

        bullet = bulletCombo;
    }
}
