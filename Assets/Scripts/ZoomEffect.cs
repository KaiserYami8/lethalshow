﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ZoomEffect : MonoBehaviour
{
    public bool[] zoomPlayer;

    public GameObject[] players;

    public RectTransform rect;

    public float timer;
    public float time;

    public bool trigger = false;

    void Update()
    {
        for (int i = 0; i < 2; i++)
        {
            if (zoomPlayer[i] && !trigger)
            {
                /*transform.position = new Vector3(0, .5f, 7f);
                transform.eulerAngles = new Vector3(0, -90, 0);
                rect.DOLocalMoveX(-3600, 0);*/

                FindObjectOfType<TimeManager>().enabled = false;
                Time.timeScale = .01f;
                timer = 1f;

                Debug.Log(Time.deltaTime);

                trigger = true;

                return;
            }
            if (trigger && zoomPlayer[i])
            {
                //transform.DOMove(players[i].transform.position, .2f * Time.deltaTime);
                //transform.DORotate(new Vector3(0, 0, 0), .2f * Time.deltaTime);

                transform.GetChild(0).gameObject.SetActive(true);
                //rect.DOLocalMoveX(0, .2f * Time.deltaTime);

                Sequence mySequence = DOTween.Sequence();
                mySequence.Append(transform.DOMove(players[i].transform.position, time));
                mySequence.Join(transform.DORotate(new Vector3(0, 0, 0), time));
                mySequence.Join(rect.DOLocalMoveX(0, time));

                zoomPlayer[i] = false;
                trigger = false;
            }
        }

        if (Time.timeScale == .01f)
        {
            timer -= Time.unscaledDeltaTime;

            if (timer <= 0)
            {
                /*rect.DOLocalMoveX(3600, .2f);
                transform.DOMove(new Vector3(0, .5f, 7f), .2f);
                transform.DORotate(new Vector3(0, 180, 0), .2f);*/

                Sequence endSequence = DOTween.Sequence();
                endSequence.Append(rect.DOLocalMoveX(3600, .2f));
                endSequence.Join(transform.DOMove(new Vector3(30, 10f, 7f), .2f));
                endSequence.Join(transform.DORotate(new Vector3(0, 180, 0), .2f));

                FindObjectOfType<TimeManager>().enabled = true;
                Time.timeScale = 1f;
            }
        }
    }
}
