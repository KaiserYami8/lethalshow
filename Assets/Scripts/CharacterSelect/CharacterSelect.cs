﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class CharacterSelect : MonoBehaviour
{
    public int controllerIndex;
    public char controllerType;

    float inputDelay;
    bool isMoving;

    public RectTransform cursor;
    int cursorIndX, cursorIndZ;

    public CharaLine[] charArray;

    public GameObject Background_CP, CP, CPTransition, Nom_CP, ScreenFade, ReadyToFight;
    public Sprite[] sCP, sNom_CP;
    public Color[] BackgroundColor;

    Vector3 originPosCP, originPosCPTransition;

    int index;
    bool isSelect;

    private void Awake()
    {
        //FindObjectOfType<AudioScript>().ChangeVolume("IntroLoop", .5f);
    }

    void Start()
    {
        //FindObjectOfType<AudioScript>().ChangeVolume("IntroLoop", .5f);

        isMoving = false;
        cursorIndX = controllerIndex-1;
        cursorIndZ = 0;

        originPosCP = CP.GetComponent<RectTransform>().position;
        originPosCPTransition = CPTransition.GetComponent<RectTransform>().position;

        CPTransition.GetComponent<RectTransform>().position = originPosCP;

        //SetCursor();
    }
    
    void Update()
    {
        //Selection
        if (!isSelect)
        {
            inputDelay -= Time.deltaTime;
            int x = Mathf.RoundToInt(Input.GetAxis("P" + controllerIndex + controllerType + "_HMove"));
            int z = -Mathf.RoundToInt(Input.GetAxis("P" + controllerIndex + controllerType + "_VMove"));

            if ((Mathf.Abs(x) + Mathf.Abs(z)) >= 1f && inputDelay <= 0)
            {
                int diffX = cursorIndX;
                int diffZ = cursorIndZ;

                cursorIndZ = Mathf.Clamp(cursorIndZ + z, 0, charArray.Length - 1);
                if (z != 0 && x == -1) x = 0;
                cursorIndX = Mathf.Clamp(cursorIndX + x, 0, charArray[cursorIndZ].charas.Length - 1);

                if (diffX != cursorIndX || diffZ != cursorIndZ)
                    SetCursor();

                if (!isMoving)
                    inputDelay = 1.2f;
                else
                    inputDelay = .2f;

                isMoving = true;
            }
            if ((Mathf.Abs(x) + Mathf.Abs(z)) == 0)
            {
                inputDelay = 0;
                isMoving = true;
            }
        }

        //Select
        if (Input.GetButtonDown("P" + controllerIndex + controllerType + "_Shoot") && cursorIndZ == 0)
        {
            if (!isSelect)
                Select();
            else if (IsReady())
            {
                FindObjectOfType<AudioScript>().Stop("IntroLoop");

                FindObjectOfType<LevelLoader>().LoadLevel(2);
            }
        }
        //Cancel
        if (Input.GetButtonDown("P" + controllerIndex + controllerType + "_Lob") && isSelect)
        {
            Cancel();
        }
    }

    void SetCursor()
    {
        //Place Cursor
        cursor.position = charArray[cursorIndZ].charas[cursorIndX].position;
        if (cursorIndZ == 0)
            index = (cursorIndX);
        if (cursorIndZ == 1)
            index = (cursorIndX + 2);

        //Switch Preview character (move, fade)

        //Initialize
        CP.GetComponent<Image>().sprite = CPTransition.GetComponent<Image>().sprite;
        CPTransition.GetComponent<Image>().sprite = sCP[index];
        CP.GetComponent<RectTransform>().position = originPosCP;
        CPTransition.GetComponent<RectTransform>().position = originPosCPTransition;
        CP.GetComponent<Image>().DOFade(1, 0);
        //Tween
        CP.GetComponent<RectTransform>().DOMoveX(960, .2f);
        CP.GetComponent<Image>().DOFade(0, .2f);
        CPTransition.GetComponent<RectTransform>().DOMoveX(originPosCP.x, .4f);

        //Set Name & Color
        Background_CP.GetComponent<Image>().color = BackgroundColor[index];
        Nom_CP.GetComponent<Image>().sprite = sNom_CP[index];

        FindObjectOfType<AudioScript>().Play("MoveCharacter");
    }

    void Select()
    {
        //Select Character (ghost, lighten)

        //Initialize
        isSelect = true;
        CP.GetComponent<RectTransform>().position = originPosCP;
        CP.GetComponent<Image>().sprite = CPTransition.GetComponent<Image>().sprite;
        CP.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        CPTransition.GetComponent<Image>().color = new Color(1, 1, 1, 1);
        CP.GetComponent<RectTransform>().DOScale(1, 0);

        //Tween
        CP.GetComponent<RectTransform>().DOScale(2, .2f);
        CP.GetComponent<Image>().DOFade(0, .2f);

        FindObjectOfType<AudioScript>().Play("SelectCharacter");

        //Set GlobalScript Player
        FindObjectOfType<GlobalScript>().playerChara[controllerIndex - 1] = charArray[cursorIndZ].charas[cursorIndX].name;

        //Show ReadyToFight Text
        if (IsReady())
        {
            //Initialize
            ReadyToFight.GetComponent<RectTransform>().position = new Vector3(-Camera.main.pixelWidth * .5f, Camera.main.pixelHeight * .5f, 0);
            //Tween
            ScreenFade.GetComponent<Image>().DOFade(.3f, .1f);
            ReadyToFight.GetComponent<RectTransform>().DOMoveX(Camera.main.pixelWidth * .5f, .1f);

            FindObjectOfType<AudioScript>().Play("ReadyToFight");
        }
    }

    void Cancel()
    {
        //Cancel Selction (unlighten)
        CP.GetComponent<RectTransform>().position = originPosCPTransition;
        CP.GetComponent<RectTransform>().DOScale(1, 0);
        CPTransition.GetComponent<Image>().color = new Color(.5f, .5f, .5f, 1);
        CP.GetComponent<Image>().color = new Color(.5f, .5f, .5f, 1);

        //Cancel ReadyToFight Text
        if (IsReady())
        {
            ScreenFade.GetComponent<Image>().DOFade(0, .1f);
            ReadyToFight.GetComponent<RectTransform>().DOMoveX(Camera.main.pixelWidth * 1.5f, .1f);
        }

        isSelect = false;

        FindObjectOfType<AudioScript>().Play("DeselectCharacter");
    }

    bool IsReady()
    {
        //Find if all are Ready
        foreach(CharacterSelect cs in FindObjectsOfType<CharacterSelect>())
            if (!cs.isSelect)
                return false;

        return true;
    }
}

[System.Serializable]
public class CharaLine
{
    public RectTransform[] charas;
}