﻿using System;
using UnityEngine;
using UnityEngine.Audio;

public class AudioScript : MonoBehaviour
{
    public Sound[] sounds;

    public static AudioScript instance;

    AudioSource[] allAudioSrc;

    void Awake()
    {
        //Set Instance
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            //return to avoid reading code after delete
            return;
        }

        DontDestroyOnLoad(gameObject);

        allAudioSrc = new AudioSource[sounds.Length];

        for (int i = 0; i < sounds.Length; i++)
        {
            var s = sounds[i];

            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.outputAudioMixerGroup = s.mixerGrp;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;

            allAudioSrc[i] = s.source;
        }
    }

    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not found !");
            return;
        }
        s.source.Play();
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not found !");
            return;
        }
        s.source.Stop();
    }

    public void ChangeVolume(string name, float volume)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not found !");
            return;
        }
        s.source.volume = volume;
    }

    public void StopAllSounds()
    {
        foreach(AudioSource s in allAudioSrc)
        {
            s.Stop();
        }
    }
}

[System.Serializable]
public class Sound
{
    public string name;

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume = 1;
    [Range(.1f, 3f)]
    public float pitch = 1;

    public bool loop;

    [HideInInspector]
    public AudioSource source;

    public AudioMixerGroup mixerGrp;
}