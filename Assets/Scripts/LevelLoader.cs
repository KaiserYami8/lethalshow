﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public Image toot;

    //Start Loading
    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadAsynchronously(sceneIndex));

        GetComponent<CanvasGroup>().alpha = 1;
    }

    IEnumerator LoadAsynchronously (int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        while(!operation.isDone)
        {
            //Show Loading
            float progress = Mathf.Clamp01(operation.progress / .9f);

            //toot.fillAmount = 1;

            yield return null;
        }
    }
}