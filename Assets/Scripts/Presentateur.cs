﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Presentateur : MonoBehaviour
{
    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void Anim(string animName, bool isTalking, bool react)
    {
        anim.SetBool("Bored", false);
        anim.SetBool("Shock", false);
        anim.SetBool("Ouch", false);
        anim.SetBool("Happy", false);
        anim.SetBool("Disappointed", false);
        anim.SetBool("Tense", false);

        if (animName != "Idle")
            anim.SetBool(animName, true);
        anim.SetBool("Talking", isTalking);
        if (react)
            anim.SetTrigger("Reaction");
    }

    public void Sound(string soundName)
    {
       // if(nb == 0)
            FindObjectOfType<AudioScript>().Play(soundName);
            
        //if(nb > 0)
          //  FindObjectOfType<AudioScript>().Play(soundName + Random.Range(1, nb+1));
    }
}