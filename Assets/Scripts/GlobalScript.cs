﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalScript : MonoBehaviour
{
    public string[] playerChara;

    public static GlobalScript instance;

    float timeSinceLastInput;

    void Awake()
    {
        //Set Instance
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            //return to avoid reading code after delete
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Update()
    {
        timeSinceLastInput += Time.deltaTime;
        if (Input.anyKeyDown)
            timeSinceLastInput = 0;

        if (timeSinceLastInput > 30)
        {
            timeSinceLastInput = 0;
            FindObjectOfType<AudioScript>().StopAllSounds();
            FindObjectOfType<LevelLoader>().LoadLevel(0);
        }
    }
}