﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroManager : MonoBehaviour
{
    bool nextAvailable = false;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            if (nextAvailable)
            {
                FindObjectOfType<AudioScript>().ChangeVolume("IntroLoop", .5f);
                FindObjectOfType<LevelLoader>().LoadLevel(1);
            }
            else
            {
                //GetComponent<Animator>().SetTrigger("Skip");
                //LoopSound();
            }
        }
    }

    public void IntroSound()
    {
        FindObjectOfType<AudioScript>().Play("Intro");
    }

    public void LoopSound() 
    {
        FindObjectOfType<AudioScript>().Stop("Intro");
        FindObjectOfType<AudioScript>().Play("IntroLoop");
        nextAvailable = true;
    }
}
