﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyController : MonoBehaviour
{
    public float spd;

    //[HideInInspector]
    public int index;

    //[HideInInspector]
    public Transform[] trackSpawn, trackLoop;

    public Transform[] trackToFollow;

    public EnemyShoot es;

    public bool isPelush;

    void OnEnable()
    {
        trackToFollow = trackSpawn;
        index = 0;
        if (trackToFollow.Length != 0)
            transform.position = trackToFollow[index].position;
    }
    
    void Update()
    {
        FollowTrack();
    }

    public void FollowTrack()
    {
        if (trackToFollow.Length > 0)
        {
            float d = Vector3.Distance(transform.position, trackToFollow[index].position);

            if (d <= .4f)
            {
                index++;
                if (trackToFollow == trackLoop)
                    index = index % (trackLoop.Length);
                d = Vector3.Distance(transform.position, trackToFollow[index].position);

                if (trackToFollow == trackSpawn)
                    transform.DOMove(trackToFollow[index].position, d * (.1f / spd)).SetEase(Ease.Linear);
                if (trackToFollow == trackLoop)
                    transform.DOMove(trackToFollow[index].position, d * (1f / spd)).SetEase(Ease.InOutSine);

                if (index == trackToFollow.Length - 1 && trackToFollow == trackSpawn)
                {
                    trackToFollow = trackLoop;
                    index = 0;
                    if (es != null)
                        es.enabled = true;
                }
            }
        }
    }
}
